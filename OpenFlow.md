Openflow je jedným z prvých protokolov, umožňujúcich komunikáciu medzi aktívnymi prvkami siete a SDN kontrolérom, ktorý ho plne ovláda. Základ protokolu bol navrhnutý na Standfordskej univerzite v roku 2008. Základným chovaním protokolu je, že pokiaľ prijme paket, ktorý nie je doposiaľ známy, je predaný kontroléru k spracovaniu. Ten rozhodne, čo sa má s daným paketom ďalej spraviť a aktívny prvok si založí tento záznam do svojej tabuľky. Keď teda neskôr príde rovnaký paket, respektíve paket, ktorý má rovnaké sledovacie znaky, už vie, čo má s ním spraviť a v kontroléri sa už nezobrazuje, aby nedochádzalo k zbytočným oneskoreniam v komunikácii. Toto chovanie je možné samozrejme modifikovať, niektoré pakety môžu byť kontrolérom zasielané napríklad neustále aj za väčšiu cenu réžie v sieti.

# Princíp OpenFlow
Nasledujúci obrázok znázorňuje, čo sa stane s paketom, ktorý príde do OpenFlow prepínača. 

![Princíp OpenFlow](https://bytebucket.org/palo73/2017-integraciasieti/raw/1061dd73fa748c03d0e2a2d70e9166c07eee9de0/Team%205%20-%20Tuka/cvi%C4%8Denia/obrazky/princip.png?token=1d7ba0cebec7daed1ffbbf20a98bae8818e5dd68 "Princíp OpenFlow")

**Krok 0**

V tomto bode je nastavené spojenie medzi OpenFlow kontrolérom a OpenFlow prepínačom. V obrázku je znázornené spojenie 1:1 a jedná sa o TCP spojenie.

**Krok 1**

Tento krok vysvetľuje, čo sa stane s prichádzajúcim paketom. Prichádzajúci paket môže byť kontrolný alebo dátový a bude odovzdaný do OpenFlow klientskeho modulu (server modul je na kontoléri), ktorý beží v prepínači.

**Krok2**

V tomto kroku sa OpenFlow prepínač stará o fyzické spracovanie paketu, ktorý je špecifický pre rôznych výrobcov prepínačov a a tiež záleží na type spojenia.

**Krok 3**

Akonáhle je paket prijme prepínač pre spracovanie, OpenFlow klient, ktorý beží v prepínači analyzuje hlavičky paketu, aby sa zistilo, o aký typ paketu sa jedná. To sa obvykle deje extrahovaním jednej z dvanástich n-tíc (tubes) z paketu a porovnávaním so záznamami z flow tabuľky.

**Krok 4 a 5**

V tomto kroku prebieha porovnávanie so záznamami z flow tabuľky, od prvého až po posledný záznam. Pokiaľ sa daný záznam našiel, zistí sa možná akcia pre tento paket. Existujú rôzne implementácie prehľadávania flow tabuľky, takže niektoré sú rýchlejšie a efektívnejšie než iné.

**Krok 6**

Pokiaľ je záznam nájdený, tak sa vykoná daná akcia. Typicky to môže byť ďalšie smerovanie paketu, zahodenie paketu alebo zaslanie kontroléru. Pokiaľ nie je žiadna akcia nájdená, tak je paket zahodený. Pokiaľ nie je nájdený žiadny záznam, potom je paket zapuzdrený a poslaný kontroléru.

**Krok 7 a 8**

Paket je poslaný do kontroléru pre určenie ďalšej akcie. Je zapuzdrený v tzv. paket-in, a jedná sa o špeciálny paket určený pre komunikáciu s kontrolérom.

**Krok 9**

Na základe konfigurácie kontroléru je rozhodnuté, čo sa spraví s paketom. S paketom sa nemusí stať vôbec nič (je zahodený), alebo môže byť inštalovaná alebo upravená flow tabuľka v OpenFlow prepínači. Tiež to môže byť paket pre komunikáciu s kontrolérom, ktorý je následne spracovaný kontrolérom.

**Krok 10**

Pokiaľ kontrolér rozhodne o pridaní alebo úprave flow tabuľky, tak je poslaná relevantná kontrolná správa OpenFlow prepínaču.
Pokiaľ príde rovnaký paket (má rovnakú hlavičku), tak sa vykoná akcia podľa kroku 6.

### OpenFlow komponenty:
-	OpenFlow kontrolér
-	OpenFlow přepínač 
-	OpenFlow Protokol

![komponenty](https://bytebucket.org/palo73/2017-integraciasieti/raw/1061dd73fa748c03d0e2a2d70e9166c07eee9de0/Team%205%20-%20Tuka/cvi%C4%8Denia/obrazky/komponenty.png?token=5baeaef55bb921dfc1e7fbccaad3e90e6367f9ae "Komponenty")

## OpenFlow kontrolér
OpenFlow kontrolér je jadro každej softwarovo definovanej siete založenej na OpenFlow protokole. Je to software, na ktorom beží riadiaca vrstva. Programuje a konfiguruje flow tabuľky prepínača (napríklad výpočet najkratšej cesty je vypočítaná v kontroléri a potom sú naprogramované príslušné prepínače). Kontrolér beží na výkonnom serveri, ktorý by mal byť v dosahu všetkých OpenFlow prepínačov. Pre jednoduché použitie užívateľom alebo administrátorom je ovládanie sprístupnené cez GUI.

Pokiaľ nastane nejaká nová udalosť, ktorú OpenFlow prepínače nemajú vo svojej flow tabuľke, tak sa pýtajú kontroléra. Súčasné kontroléry majú schopnosť zobraziť v GUI celú sieť a mnoho ďalších z nich dokáže interaktívne zobraziť aktuálny dátová tok v sieti.

## OpenFlow přepínač 
Sú to obyčajné prepínače, len s tým rozdielom, že neobsahujú „inteligentné“ protokoly. Obsahujú len základ, aby boli schopní sa pripojiť k OpenFlow kontroléru a inštalovať a analyzovať dátové toky. V súčasnej dobe je definícia OpenFlow prepínačov špecifikovaná podľa zákazníka. Na najvyššej úrovni ich možno rozdeliť na:
-	Čistý OpenFlow prepínač, ktorý podporuje len OpenFlow protokol
-	Hybridný OpenFlow prepínač, ktorý podporuje okrem OpenFlow protokolu aj ďalšie tradičné ethernetové porotokoly.

Bez ohľadu na typ prepínača, OpenFlow prepínač bude mať modul pre nadviazanie spojenia (SSH/TCP) s OpenFlow kontrolérom a flow tabuľkou, ktorá je konfigurovaná kontrolérom na základe jeho implementácie.



Aktuálna verzia protokolu je č. 1.5 pričom vždy po pripojení aktívneho prvku kú kontrolóru dôjde k vyhľadávaniu najvyššej obojstranne podporovanej verzii protokolu. Kontrolér teda napríklad zvláda verziu 1.0, 1.3 a 1.5, ale aktívny prvok len 1.0 a 1.3. Dôjde teda ku komunikácii vo verzii 1.3 (vybraná je verzia 1.3). 

Protokol umožňuje vkladanie 3 typov záznamov do aktívneho prvku:
- Pravidlá – určujú, akých paketov sa budú určité akcie týkať.
- Akcie – operácie, čo sa s daným paketom urobí.
- Štatistiky – štatistiky o spracovaní paketov a sieťových zdrojov.

## Flow tabuľka
Flow tabuľka je v OpenFlow prepínači a určuje, čo sa robí z prichádzajúcimi paketmi. OpenFlow kontrolér naplní flow tabuľku na základe rôznych tokov a fyzickej topológie siete. Všetok prichádzajúci tok dát do OpenFlow prepínača je spracovaný na základe flow tabuľky. Pokial nie je nájdený žiaden zodpovedajúci záznam, čo sa má urobiť z prichádzajúcim paketom, je poslaný do kontroléra, aby bolo rozhodnuté čo s ním spraviť. Akonáhle je rozhodnuté, flow tabuľka sa aktualizuje.

![Flow tabulka](https://bytebucket.org/palo73/2017-integraciasieti/raw/1061dd73fa748c03d0e2a2d70e9166c07eee9de0/Team%205%20-%20Tuka/cvi%C4%8Denia/obrazky/tabulka.png?token=2a48a60ef7b26dc60a1bbcce7dda4730d5cfa358 "Flow tabulka")

Aktuálna verzia protokolu je č. 1.5 pričom vždy po pripojení aktívneho prvku kú kontroléru dôjde k vyhľadávaniu najvyššej obojstranne podporovanej verzii protokolu. Kontrolér teda napríklad zvláda verziu 1.1, 1.3 a 1.5, ale aktívny prvok len 1.1 a 1.3. Dôjde teda ku komunikácii vo verzii 1.3 (vybraná je verzia 1.3). 

**Protokol umožňuje vkladanie 3 typov záznamov do aktívneho prvku:**
-	Pravidlá – určujú, akých paketov sa budú určité akcie týkať
-	Akcie – operácie, čo sa s daným paketom urobí
-	Štatistiky – štatistiky o spracovaní paketov a sieťových zdrojov

![Typy zaznamov](https://bytebucket.org/palo73/2017-integraciasieti/raw/1061dd73fa748c03d0e2a2d70e9166c07eee9de0/Team%205%20-%20Tuka/cvi%C4%8Denia/obrazky/typyzaznamov.jpg?token=fa7e51e60efdc2c5604fee37099d27902ed3fe64 "Typy zaznamov")

**Vkladanie záznamov do tabuliek môže prebiehať dvoma spôsobmi:**

-	Relatívne – po prijatí prvku kontrolérom je tabuľka prázdna a postupne si plní záznamy. To znamená, že na začiatku sa prvok pýta kontroléru na každý paket. Nevýhodou sú väčšie doby odozvy na začiatku, než sa sieť naučí smerovanie prevádzky. Naopak výhodou je nezahltenie siete ihneď po jej spustení.
-	Proaktívne – po pripojení prvku ku kontroléru, kedy dôjde k naplneniu jeho tabuľky. Pri spracovaní paketu tak už vie, čo má s ním spraviť. Tú dochádza k prvotnej rozsiahlej komunikácii medzi kontrolérom a riadiacim prvkom. Po tomto kroku väčšinou ku komunikácii už nedochádza.

Prípadne sa môže použiť aj ich kombinácia, kedy dôjde po pripojení prvku ku kontroléru k určitému naplneniu pravidiel, nie však kompletnému a zbytok sa prvok doučí v priebehu, v prípade, že tieto záznamy bude potrebovať.

### OpenFlow porty
OpenFlow definuje niekoľko typov portov pre prepínače.


- Fyzické porty – tieto porty sú fyzické porty na prepínači
- Logické porty – môžu byť použité napríklad pre VLAN porty alebo loopback
- Rezervované porty – tieto porty majú špeciálne využitie pre preposielacie akcie. Patrí sem **CONTROLLER port**, ktorý je určený pre odosielanie paketov na kontrolér, **Table port** používaný kontrolérom, keď kontrolér odošle paket na prepínač a chce spracovať paket cez flow tabuľku v prepínači a **IN_PORT** môže sa používať na odosielanie paketu na vstupný port na prepínači